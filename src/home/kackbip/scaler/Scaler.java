package home.kackbip.scaler;

import com.sun.corba.se.impl.logging.ORBUtilSystemException;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * User: ryashentsev
 * Date: 08.05.13
 * Time: 10:15
 * To change this template use File | Settings | File Templates.
 */
public class Scaler {

    public static void main(String[] args){
        try{
            if(/*args.length>0*/true){

                String sourceDirPath = "drawable-xhdpi";//args[0];
                File sourceDir = new File(sourceDirPath);
                String[] files = sourceDir.list();
                File mdpiDir = new File("drawable-mdpi");
                if(!mdpiDir.exists()) mdpiDir.mkdir();
                File hdpiDir = new File("drawable-hdpi");
                if(!hdpiDir.exists()) hdpiDir.mkdir();
                float mdpiScale = 0.5f;
                float hdpiScale = 0.67f;
                BufferedImage sourceImage;
                for(String filename: files){
                    System.out.println(filename);
                    sourceImage = ImageIO.read(new File(sourceDir, filename));
                    if(sourceImage==null){
                        System.out.println(filename + "skipped");
                        continue;
                    }
                    scaleFileAndSave(sourceImage, filename, mdpiDir, mdpiScale);
                    scaleFileAndSave(sourceImage, filename, hdpiDir, hdpiScale);
                    sourceImage.getGraphics().dispose();
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void scaleFileAndSave(BufferedImage sourceImage, String filename, File dirToSave, float scale) throws IOException{
        BufferedImage resultImage = scaleBufferedImage(sourceImage, scale);
        File outputfile = new File(dirToSave, filename);
        ImageIO.write(resultImage, "png", outputfile);
        resultImage.getGraphics().dispose();
    }

    private static BufferedImage scaleBufferedImage(BufferedImage bImg, float scale){
        int width = (int) (bImg.getWidth() * scale);
        int height = (int) (bImg.getHeight() * scale);
        Image image = bImg.getScaledInstance(width, height, Image.SCALE_SMOOTH);
        BufferedImage result = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
        result.getGraphics().drawImage(image, 0, 0 , null);
        return result;
    }

}
